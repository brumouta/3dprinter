import json
from os import getenv


def main(event, context):

    ENERGY_PER_HOUR = float(getenv("ENERGY_PER_HOUR"))
    DEPRECIATION = float(getenv("DEPRECIATION"))
    MATERIAL_COST = float(getenv("MATERIAL_COST"))
    LABOR_COST = float(getenv("LABOR_COST"))
    MARKUP = float(getenv("MARKUP"))
    CONTINGENCY = float(getenv("CONTINGENCY", 0.1))

    event = json.loads(event.get("body"))

    weight = float(event.get("weight"))
    hours_labor = float(event.get("hours_labor"))
    hours_printing = float(event.get("hours_printing"))

    energy_cost = (ENERGY_PER_HOUR * hours_printing) * (1 + CONTINGENCY)
    material_cost = (MATERIAL_COST * weight / 1000) * (1 + CONTINGENCY)
    depreciation_cost = (DEPRECIATION * hours_printing)
    labor_cost = (LABOR_COST * hours_labor)
    total_cost = energy_cost + material_cost + depreciation_cost + labor_cost
    profit = total_cost * MARKUP

    body = {
        "total_cost": round(total_cost, 2),
        "profit": round(profit, 2),
        "price": round(total_cost + profit, 2),
        "input": event
    }

    response = {
        "statusCode": 200,
        "isBase64Encoded": False,
        "headers": {
            'Content-Type': 'application/json'
        },
        "body": json.dumps(body)
    }

    return response
